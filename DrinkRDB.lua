local addonName, DDB = ...
[[--
local addonName, DDB = ..
This passes the DDB between files which contains the 
drink_table 	- this houses the drinking options saved
isMacro 		- this method refreshes the amcro and saves the current setting to DrinkRDB
holder			- this is the main frame that holds the scrollable frames
slScrollFrame	- this contains the parent frame of slScrollFrame which can be manipulated this way in other files 
mageScrollFrame	- this contains the parent frame of mageScrollFrame which can be manipulated this way in other files 
--]]

local f = CreateFrame("Frame")

-- Initializing a table for the drinks --
f.defaults = {
	-- MAGE --
	cmbOption 	= {true, "Conjured Mana Bun"},
	cmpOption 	= {true, "Conjured Mana Pudding"},
	-- SHADOWLANDS --
	adOptions 	= {true, "Ambroria Dew"},
	abtOptions 	= {true, "Azurebloom Tea"},
	batOptions 	= {true, "Bone Apple Tea"},
	cosOptions 	= {true, "Circle of Subsistence"},
	capOptions 	= {true, "Catalyzed Apple Pie"},
	efsOptions 	= {true, "Empyrean Fruit Salad"},
	etOptions 	= {true, "Ethereal Pomegranate"},
	ltmOptions 	= {true, "Lukewarm Tauralus Milk"},
	rfOptions 	= {true, "Restorative Flow"},
	swOptions 	= {true, "Shadespring Water"},
	ssOptions 	= {true, "Stygian Stew"},
	sssOptions 	= {true, "Suspicious Slime Shot"},
	bjOptions 	= {true, "Beetle Juice"},
	csOptions 	= {true, "Corpini Slurry"},
	ccOptions 	= {true, "Cranial Concoction"},
	foaOptions 	= {true, "Flask of Ardendew"},
	imwOptions 	= {true, "Infused Muck Water"},
	pswOptions 	= {true, "Purified Skyspring Water"},
	sasOptions 	= {true, 'Sable "Soup"'},
	slwOptions 	= {true, "Slushy Water"},
	-- LEGION --
}

-- Loading in saved table from saved_variables or the default if there is no saved --
function f:InitDb(event, addOnName)
	if addOnName == "DrinkR" then
		DDB.drink_table = DrinkRDB or CopyTable(self.defaults)
		--DDB.drink_table = DrinkRDB_loaded
	end
end

-- Registering events to fire InitDB on --
f:RegisterEvent("ADDON_LOADED")
f:RegisterEvent("PLAYER_LOGOUT")
f:SetScript("OnEvent", f.InitDb)