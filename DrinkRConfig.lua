local addonName, DDB = ...

-- Registering frames for eventhandling --
local f = CreateFrame("Frame")

function f:OnEvent(event, addOnName)
	if addOnName == "DrinkR" then
		self.db = DDB.drink_table
		self:InitializeOptions()
	end
end

f:RegisterEvent("ADDON_LOADED")
f:RegisterEvent("PLAYER_LOGOUT")
f:SetScript("OnEvent", f.OnEvent)

-- This function clears all frames from handler2 --
function hideAllFood()
	DDB.slScrollFrame:Hide()
	DDB.mageScrollFrame:Hide()
end

-------------------- Create addon options panel -----------------------------------------

function f:InitializeOptions()
	self.panel = CreateFrame("Frame")
	self.panel.name = "DrinkR"
	
	----------------------------- MAIN HOLDERS ------------------------------------------
	
	-- Holder for DrinkR name --
	local name_holder = CreateFrame("Frame", nil, self.panel)
	name_holder:SetWidth(200) 
	name_holder:SetHeight(30) 
	name_holder:SetPoint("TOPLEFT", 10, -10)
	name_holder.text = name_holder:CreateFontString(nil,"ARTWORK") 
	name_holder.text:SetFont("Fonts\\MORPHEUS.ttf", 20, "OUTLINE")
	name_holder.text:SetPoint("CENTER", 0, 0)
	name_holder.text:SetText("DrinkR")
	
	-- Holder for options like Mage Drinks and Shadowlands --
	local holder1 = CreateFrame("Frame", nil, self.panel, "BackdropTemplate")
	holder1:SetPoint("TOPLEFT", 10, -50)
	holder1:SetSize(200, 510)
	holder1:SetBackdrop({
		bgFile = "Interface/Tooltips/UI-Tooltip-Background",
		edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
		edgeSize = 16,
		insets = { left = 4, right = 4, top = 4, bottom = 4 },
	})
	holder1:SetBackdropColor(0.1, 0.1, 0.1, 0.5)
	
	-- Holder for the actual drinks frames --
	local holder2 = CreateFrame("Frame", nil, self.panel, "BackdropTemplate")
	holder2:SetPoint("TOPLEFT", 210, -50)
	holder2:SetSize(400, 510)
	holder2:SetBackdrop({
		bgFile = "Interface/Tooltips/UI-Tooltip-Background",
		edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
		edgeSize = 16,
		insets = { left = 4, right = 4, top = 4, bottom = 4 },
	})
	holder2:SetBackdropColor(0.1, 0.1, 0.1, 0.5)
	
	DDB.holder = holder2
	
	----------------------------- EXPANSIONS ------------------------------------------
	
	-- Mage button in holder1 --
	local mage_btn = CreateFrame("Button", nil, holder1, "UIPanelButtonTemplate")
	mage_btn:SetPoint("TOPLEFT", 5, -5)
	mage_btn:SetText("Mage Foods")
	mage_btn:SetWidth(190)
	mage_btn:SetScript("OnClick", function()
		hideAllFood()
		DDB.mageScrollFrame:Show()
	end)
	
	-- Shadowlands button in holder1 --
	local sl_btn = CreateFrame("Button", nil, mage_btn, "UIPanelButtonTemplate")
	sl_btn:SetPoint("CENTER", 0, -22)
	sl_btn:SetText("Shadowlands Foods")
	sl_btn:SetWidth(190)
	sl_btn:SetScript("OnClick", function()
		hideAllFood()
		DDB.slScrollFrame:Show()
	end)


	InterfaceOptions_AddCategory(self.panel)
end

-- Slash commands --
SLASH_DRINKR1 = "/dr"
SLASH_DRINKR2 = "/drinkr"

SlashCmdList.DRINKR = function(msg, editBox)
	-- https://github.com/Stanzilla/WoWUIBugs/issues/89
	InterfaceOptionsFrame_OpenToCategory(f.panel)
	InterfaceOptionsFrame_OpenToCategory(f.panel)
end