local addonName, DDB = ...

-- Registering frames for eventhandling --
local f = CreateFrame("Frame")

function f:OnEvent(event, addOnName)
	if addOnName == "DrinkR" then
		self.db = DDB.drink_table
		main_holder = DDB.holder
		self:InitializeOptions()
	end
end

f:RegisterEvent("ADDON_LOADED")
f:RegisterEvent("PLAYER_LOGOUT")
f:SetScript("OnEvent", f.OnEvent)

function f:InitializeOptions()

	-- This is the scrolling frames which houses the drinking options --
	local mageScrollFrame = CreateFrame("ScrollFrame", nil, main_holder, "UIPanelScrollFrameTemplate")
	mageScrollFrame:SetPoint("TOPLEFT", 3, -4)
	mageScrollFrame:SetPoint("BOTTOMRIGHT", -27, 4)
	
	DDB.mageScrollFrame = mageScrollFrame
	
	local mageScrollFrameChild = CreateFrame("Frame")
	mageScrollFrame:SetScrollChild(mageScrollFrameChild)
	mageScrollFrameChild:SetWidth(InterfaceOptionsFramePanelContainer:GetWidth()-18)
	mageScrollFrameChild:SetHeight(1) 

	------------------------------ CONJURED MANA BUN -------------------------------------------------

	local conjured_mana_bun = CreateFrame("Frame", nil, mageScrollFrameChild)
	conjured_mana_bun:SetPoint("TOPLEFT", 10, -10)
	conjured_mana_bun:SetSize(32, 32)
	conjured_mana_bun.tex = conjured_mana_bun:CreateTexture()
	conjured_mana_bun.tex:SetAllPoints(conjured_mana_bun)
	conjured_mana_bun.tex:SetTexture("134029")
	
	conjured_mana_bun:SetScript("OnEnter", function()
		GameTooltip:SetOwner(conjured_mana_bun, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:113509")
		GameTooltip:Show()
		end)
	conjured_mana_bun:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local cmb_cb = CreateFrame("CheckButton", nil, conjured_mana_bun, "InterfaceOptionsCheckButtonTemplate")
	cmb_cb:SetPoint("CENTER", 28, 0)
	cmb_cb.Text:SetText("Conjured Mana Bun")
	cmb_cb.SetValue = function(_, value)
		self.db.cmbOption[1] = (value == "1")
		DDB.isMacro()
	end
	cmb_cb:SetChecked(self.db.cmbOption[1])
	
	------------------------------ Conjured Mana Pudding -------------------------------------------------

	local conjured_mana_pudding = CreateFrame("Frame", nil, conjured_mana_bun)
	conjured_mana_pudding:SetPoint("CENTER", 0, -36)
	conjured_mana_pudding:SetSize(32, 32)
	conjured_mana_pudding.tex = conjured_mana_pudding:CreateTexture()
	conjured_mana_pudding.tex:SetAllPoints(conjured_mana_pudding)
	conjured_mana_pudding.tex:SetTexture("609813")
	
	conjured_mana_pudding:SetScript("OnEnter", function()
		GameTooltip:SetOwner(conjured_mana_pudding, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:80610")
		GameTooltip:Show()
		end)
	conjured_mana_pudding:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local cmp_cb = CreateFrame("CheckButton", nil, conjured_mana_pudding, "InterfaceOptionsCheckButtonTemplate")
	cmp_cb:SetPoint("CENTER", 28, 0)
	cmp_cb.Text:SetText("Conjured Mana Pudding")
	cmp_cb.SetValue = function(_, value)
		self.db.cmpOption[1] = (value == "1")
		DDB.isMacro()
	end
	cmp_cb:SetChecked(self.db.cmpOption[1])

end