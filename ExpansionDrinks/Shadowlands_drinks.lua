local addonName, DDB = ...

-- Registering frames for eventhandling --
local f = CreateFrame("Frame")

function f:OnEvent(event, addOnName)
	if addOnName == "DrinkR" then
		self.db = DDB.drink_table
		main_holder = DDB.holder
		self:InitializeOptions()
	end
end

f:RegisterEvent("ADDON_LOADED")
f:RegisterEvent("PLAYER_LOGOUT")
f:SetScript("OnEvent", f.OnEvent)

function f:InitializeOptions()

	-- This is the scrolling frames which houses the drinking options --
	local slScrollFrame = CreateFrame("ScrollFrame", nil, main_holder, "UIPanelScrollFrameTemplate")
	slScrollFrame:SetPoint("TOPLEFT", 3, -4)
	slScrollFrame:SetPoint("BOTTOMRIGHT", -27, 4)
	slScrollFrame:Hide()
	
	DDB.slScrollFrame = slScrollFrame
	
	local slScrollChild = CreateFrame("Frame")
	slScrollFrame:SetScrollChild(slScrollChild)
	slScrollChild:SetWidth(InterfaceOptionsFramePanelContainer:GetWidth()-18)
	slScrollChild:SetHeight(1) 

	------------------------------ AMBRORIA DEW -------------------------------------------------
	
	local ambroria_dew = CreateFrame("Frame", nil, slScrollChild)
	ambroria_dew:SetPoint("TOPLEFT", 10, -10)
	ambroria_dew:SetSize(32, 32)
	ambroria_dew.tex = ambroria_dew:CreateTexture()
	ambroria_dew.tex:SetAllPoints(ambroria_dew)
	ambroria_dew.tex:SetTexture("132830")
	
	ambroria_dew:SetScript("OnEnter", function()
		GameTooltip:SetOwner(ambroria_dew, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:177040")
		GameTooltip:Show()
		end)
	ambroria_dew:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local ad_cmb = CreateFrame("CheckButton", nil, ambroria_dew, "InterfaceOptionsCheckButtonTemplate")
	ad_cmb:SetPoint("CENTER", 28, 0)
	ad_cmb.Text:SetText("Ambroria Dew")
	ad_cmb.SetValue = function(_, value)
		self.db.adOptions[1] = (value == "1")
		DDB.isMacro()
	end
	ad_cmb:SetChecked(self.db.adOptions[1])
	
	------------------------------ AZUREBLOOM TEA -------------------------------------------------
	
	local azurebloom_tea = CreateFrame("Frame", nil, ambroria_dew)
	azurebloom_tea:SetPoint("CENTER", 0, -36)
	azurebloom_tea:SetSize(32, 32)
	azurebloom_tea.tex = azurebloom_tea:CreateTexture()
	azurebloom_tea.tex:SetAllPoints(azurebloom_tea)
	azurebloom_tea.tex:SetTexture("132814")
	
	azurebloom_tea:SetScript("OnEnter", function()
		GameTooltip:SetOwner(azurebloom_tea, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:178217")
		GameTooltip:Show()
		end)
	azurebloom_tea:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local at_cmb = CreateFrame("CheckButton", nil, azurebloom_tea, "InterfaceOptionsCheckButtonTemplate")
	at_cmb:SetPoint("CENTER", 28, 0)
	at_cmb.Text:SetText("Azurebloom Tea")
	at_cmb.SetValue = function(_, value)
		self.db.abtOptions[1] = (value == "1")
		DDB.isMacro()
	end
	at_cmb:SetChecked(self.db.abtOptions[1])
	
	------------------------------ BONE APPLE TEA -------------------------------------------------
	
	local bone_apple_tea = CreateFrame("Frame", nil, azurebloom_tea)
	bone_apple_tea:SetPoint("CENTER", 0, -36)
	bone_apple_tea:SetSize(32, 32)
	bone_apple_tea.tex = bone_apple_tea:CreateTexture()
	bone_apple_tea.tex:SetAllPoints(bone_apple_tea)
	bone_apple_tea.tex:SetTexture("132810")
	
	bone_apple_tea:SetScript("OnEnter", function()
		GameTooltip:SetOwner(bone_apple_tea, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:178545")
		GameTooltip:Show()
		end)
	bone_apple_tea:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local bat_cmb = CreateFrame("CheckButton", nil, bone_apple_tea, "InterfaceOptionsCheckButtonTemplate")
	bat_cmb:SetPoint("CENTER", 28, 0)
	bat_cmb.Text:SetText("Bone Apple tea")
	bat_cmb.SetValue = function(_, value)
		self.db.batOptions[1] = (value == "1")
		DDB.isMacro()
	end
	bat_cmb:SetChecked(self.db.batOptions[1])
	
	------------------------------ CIRCLE OF SUBSISTENCE -------------------------------------------------
	
	local circle_of_subsistence = CreateFrame("Frame", nil, bone_apple_tea)
	circle_of_subsistence:SetPoint("CENTER", 0, -36)
	circle_of_subsistence:SetSize(32, 32)
	circle_of_subsistence.tex = circle_of_subsistence:CreateTexture()
	circle_of_subsistence.tex:SetAllPoints(circle_of_subsistence)
	circle_of_subsistence.tex:SetTexture("133945")
	
	circle_of_subsistence:SetScript("OnEnter", function()
		GameTooltip:SetOwner(circle_of_subsistence, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:190881")
		GameTooltip:Show()
		end)
	circle_of_subsistence:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local cos_cmb = CreateFrame("CheckButton", nil, circle_of_subsistence, "InterfaceOptionsCheckButtonTemplate")
	cos_cmb:SetPoint("CENTER", 28, 0)
	cos_cmb.Text:SetText("Circle of Subsistence")
	cos_cmb.SetValue = function(_, value)
		self.db.cosOptions[1] = (value == "1")
		DDB.isMacro()
	end
	cos_cmb:SetChecked(self.db.cosOptions[1])
	
	------------------------------ CATALYZED APPLE PIE -------------------------------------------------
	
	local catalyzed_apple_pie = CreateFrame("Frame", nil, circle_of_subsistence)
	catalyzed_apple_pie:SetPoint("CENTER", 0, -36)
	catalyzed_apple_pie:SetSize(32, 32)
	catalyzed_apple_pie.tex = catalyzed_apple_pie:CreateTexture()
	catalyzed_apple_pie.tex:SetAllPoints(catalyzed_apple_pie)
	catalyzed_apple_pie.tex:SetTexture("651358")
	
	catalyzed_apple_pie:SetScript("OnEnter", function()
		GameTooltip:SetOwner(catalyzed_apple_pie, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:190880")
		GameTooltip:Show()
		end)
	catalyzed_apple_pie:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local cap_cmb = CreateFrame("CheckButton", nil, catalyzed_apple_pie, "InterfaceOptionsCheckButtonTemplate")
	cap_cmb:SetPoint("CENTER", 28, 0)
	cap_cmb.Text:SetText("Catalyzed Apple Pie")
	cap_cmb.SetValue = function(_, value)
		self.db.capOptions[1] = (value == "1")
		DDB.isMacro()
	end
	cap_cmb:SetChecked(self.db.capOptions[1])
	
	------------------------------ EMPYREAN FRUIT SALAD -------------------------------------------------
	
	local empyrean_fruit_salad = CreateFrame("Frame", nil, catalyzed_apple_pie)
	empyrean_fruit_salad:SetPoint("CENTER", 0, -36)
	empyrean_fruit_salad:SetSize(32, 32)
	empyrean_fruit_salad.tex = empyrean_fruit_salad:CreateTexture()
	empyrean_fruit_salad.tex:SetAllPoints(empyrean_fruit_salad)
	empyrean_fruit_salad.tex:SetTexture("1045952")
	
	empyrean_fruit_salad:SetScript("OnEnter", function()
		GameTooltip:SetOwner(empyrean_fruit_salad, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:174284")
		GameTooltip:Show()
		end)
	empyrean_fruit_salad:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local efs_cmb = CreateFrame("CheckButton", nil, empyrean_fruit_salad, "InterfaceOptionsCheckButtonTemplate")
	efs_cmb:SetPoint("CENTER", 28, 0)
	efs_cmb.Text:SetText("Empyrean Fruit Salad")
	efs_cmb.SetValue = function(_, value)
		self.db.efsOptions[1] = (value == "1")
		DDB.isMacro()
	end
	efs_cmb:SetChecked(self.db.efsOptions[1])
	
	------------------------------ ETHEREAL POMEGRANATE -------------------------------------------------
	
	local ethereal_pomegranate = CreateFrame("Frame", nil, empyrean_fruit_salad)
	ethereal_pomegranate:SetPoint("CENTER", 0, -36)
	ethereal_pomegranate:SetSize(32, 32)
	ethereal_pomegranate.tex = ethereal_pomegranate:CreateTexture()
	ethereal_pomegranate.tex:SetAllPoints(ethereal_pomegranate)
	ethereal_pomegranate.tex:SetTexture("133999")
	
	ethereal_pomegranate:SetScript("OnEnter", function()
		GameTooltip:SetOwner(ethereal_pomegranate, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:173859")
		GameTooltip:Show()
		end)
	ethereal_pomegranate:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local et_cmb = CreateFrame("CheckButton", nil, ethereal_pomegranate, "InterfaceOptionsCheckButtonTemplate")
	et_cmb:SetPoint("CENTER", 28, 0)
	et_cmb.Text:SetText("Ethereal Pomegranate")
	et_cmb.SetValue = function(_, value)
		self.db.etOptions[1] = (value == "1")
		DDB.isMacro()
	end
	et_cmb:SetChecked(self.db.etOptions[1])
	
	------------------------------ LUKEWARM TAURALUS MILK -------------------------------------------------
	
	local lukewarm_tauralus_milk = CreateFrame("Frame", nil, ethereal_pomegranate)
	lukewarm_tauralus_milk:SetPoint("CENTER", 0, -36)
	lukewarm_tauralus_milk:SetSize(32, 32)
	lukewarm_tauralus_milk.tex = lukewarm_tauralus_milk:CreateTexture()
	lukewarm_tauralus_milk.tex:SetAllPoints(lukewarm_tauralus_milk)
	lukewarm_tauralus_milk.tex:SetTexture("132815")
	
	lukewarm_tauralus_milk:SetScript("OnEnter", function()
		GameTooltip:SetOwner(lukewarm_tauralus_milk, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:178539")
		GameTooltip:Show()
		end)
	lukewarm_tauralus_milk:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local ltm_cmb = CreateFrame("CheckButton", nil, lukewarm_tauralus_milk, "InterfaceOptionsCheckButtonTemplate")
	ltm_cmb:SetPoint("CENTER", 28, 0)
	ltm_cmb.Text:SetText("Lukewarm Tauralus Milk")
	ltm_cmb.SetValue = function(_, value)
		self.db.ltmOptions[1] = (value == "1")
		DDB.isMacro()
	end
	ltm_cmb:SetChecked(self.db.ltmOptions[1])
	
	------------------------------ RESTORATIVE FLOW -------------------------------------------------
	
	local restoretive_flow = CreateFrame("Frame", nil, lukewarm_tauralus_milk)
	restoretive_flow:SetPoint("CENTER", 0, -36)
	restoretive_flow:SetSize(32, 32)
	restoretive_flow.tex = restoretive_flow:CreateTexture()
	restoretive_flow.tex:SetAllPoints(restoretive_flow)
	restoretive_flow.tex:SetTexture("132807")
	
	restoretive_flow:SetScript("OnEnter", function()
		GameTooltip:SetOwner(restoretive_flow, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:190936")
		GameTooltip:Show()
		end)
	restoretive_flow:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local rf_cmb = CreateFrame("CheckButton", nil, restoretive_flow, "InterfaceOptionsCheckButtonTemplate")
	rf_cmb:SetPoint("CENTER", 28, 0)
	rf_cmb.Text:SetText("Restorative Flow")
	rf_cmb.SetValue = function(_, value)
		self.db.rfOptions[1] = (value == "1")
		DDB.isMacro()
	end
	rf_cmb:SetChecked(self.db.rfOptions[1])
	
	------------------------------ SHADESPRING WATER -------------------------------------------------
	
	local shadespring_water = CreateFrame("Frame", nil, restoretive_flow)
	shadespring_water:SetPoint("CENTER", 0, -36)
	shadespring_water:SetSize(32, 32)
	shadespring_water.tex = shadespring_water:CreateTexture()
	shadespring_water.tex:SetAllPoints(shadespring_water)
	shadespring_water.tex:SetTexture("132827")
	
	shadespring_water:SetScript("OnEnter", function()
		GameTooltip:SetOwner(shadespring_water, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:179992")
		GameTooltip:Show()
		end)
	shadespring_water:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local sw_cmb = CreateFrame("CheckButton", nil, shadespring_water, "InterfaceOptionsCheckButtonTemplate")
	sw_cmb:SetPoint("CENTER", 28, 0)
	sw_cmb.Text:SetText("Shadespring Water")
	sw_cmb.SetValue = function(_, value)
		self.db.swOptions[1] = (value == "1")
		DDB.isMacro()
	end
	sw_cmb:SetChecked(self.db.swOptions[1])
	
	------------------------------ STYGIAN STEW -------------------------------------------------
	
	local stygian_stew = CreateFrame("Frame", nil, shadespring_water)
	stygian_stew:SetPoint("CENTER", 0, -36)
	stygian_stew:SetSize(32, 32)
	stygian_stew.tex = stygian_stew:CreateTexture()
	stygian_stew.tex:SetAllPoints(stygian_stew)
	stygian_stew.tex:SetTexture("237329")
	
	stygian_stew:SetScript("OnEnter", function()
		GameTooltip:SetOwner(stygian_stew, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:174283")
		GameTooltip:Show()
		end)
	stygian_stew:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local ss_cmb = CreateFrame("CheckButton", nil, stygian_stew, "InterfaceOptionsCheckButtonTemplate")
	ss_cmb:SetPoint("CENTER", 28, 0)
	ss_cmb.Text:SetText("Stygian Stew")
	ss_cmb.SetValue = function(_, value)
		self.db.ssOptions[1] = (value == "1")
		DDB.isMacro()
	end
	ss_cmb:SetChecked(self.db.ssOptions[1])
	
	------------------------------ SUSPICIOUS SLIME SHOT -------------------------------------------------
	
	local suspicious_slime_shot = CreateFrame("Frame", nil, stygian_stew)
	suspicious_slime_shot:SetPoint("CENTER", 0, -36)
	suspicious_slime_shot:SetSize(32, 32)
	suspicious_slime_shot.tex = suspicious_slime_shot:CreateTexture()
	suspicious_slime_shot.tex:SetAllPoints(suspicious_slime_shot)
	suspicious_slime_shot.tex:SetTexture("351507")
	
	suspicious_slime_shot:SetScript("OnEnter", function()
		GameTooltip:SetOwner(suspicious_slime_shot, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:178535")
		GameTooltip:Show()
		end)
	suspicious_slime_shot:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local sss_cmb = CreateFrame("CheckButton", nil, suspicious_slime_shot, "InterfaceOptionsCheckButtonTemplate")
	sss_cmb:SetPoint("CENTER", 28, 0)
	sss_cmb.Text:SetText("Suspicious Slime Shot")
	sss_cmb.SetValue = function(_, value)
		self.db.sssOptions[1] = (value == "1")
		DDB.isMacro()
	end
	sss_cmb:SetChecked(self.db.sssOptions[1])
	
	------------------------------ BEETLE JUICE -------------------------------------------------
	
	local beetle_juice = CreateFrame("Frame", nil, suspicious_slime_shot)
	beetle_juice:SetPoint("CENTER", 0, -36)
	beetle_juice:SetSize(32, 32)
	beetle_juice.tex = beetle_juice:CreateTexture()
	beetle_juice.tex:SetAllPoints(beetle_juice)
	beetle_juice.tex:SetTexture("461805")
	
	beetle_juice:SetScript("OnEnter", function()
		GameTooltip:SetOwner(beetle_juice, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:178538")
		GameTooltip:Show()
		end)
	beetle_juice:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local bj_cmb = CreateFrame("CheckButton", nil, beetle_juice, "InterfaceOptionsCheckButtonTemplate")
	bj_cmb:SetPoint("CENTER", 28, 0)
	bj_cmb.Text:SetText("Beetle Juice")
	bj_cmb.SetValue = function(_, value)
		self.db.bjOptions[1] = (value == "1")
		DDB.isMacro()
	end
	bj_cmb:SetChecked(self.db.bjOptions[1])
	
	------------------------------ CORPINI SLURRY -------------------------------------------------
	
	local corpini_slurry = CreateFrame("Frame", nil, beetle_juice)
	corpini_slurry:SetPoint("CENTER", 0, -36)
	corpini_slurry:SetSize(32, 32)
	corpini_slurry.tex = corpini_slurry:CreateTexture()
	corpini_slurry.tex:SetAllPoints(corpini_slurry)
	corpini_slurry.tex:SetTexture("132808")
	
	corpini_slurry:SetScript("OnEnter", function()
		GameTooltip:SetOwner(corpini_slurry, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:178534")
		GameTooltip:Show()
		end)
	corpini_slurry:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local cs_cmb = CreateFrame("CheckButton", nil, corpini_slurry, "InterfaceOptionsCheckButtonTemplate")
	cs_cmb:SetPoint("CENTER", 28, 0)
	cs_cmb.Text:SetText("Corpini Slurry")
	cs_cmb.SetValue = function(_, value)
		self.db.csOptions[1] = (value == "1")
		DDB.isMacro()
	end
	cs_cmb:SetChecked(self.db.csOptions[1])
	
	------------------------------ CRANIAL CONCOCTION -------------------------------------------------
	
	local cranial_concoction = CreateFrame("Frame", nil, corpini_slurry)
	cranial_concoction:SetPoint("CENTER", 0, -36)
	cranial_concoction:SetSize(32, 32)
	cranial_concoction.tex = cranial_concoction:CreateTexture()
	cranial_concoction.tex:SetAllPoints(cranial_concoction)
	cranial_concoction.tex:SetTexture("463489")
	
	cranial_concoction:SetScript("OnEnter", function()
		GameTooltip:SetOwner(cranial_concoction, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:178542")
		GameTooltip:Show()
		end)
	cranial_concoction:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local cc_cmb = CreateFrame("CheckButton", nil, cranial_concoction, "InterfaceOptionsCheckButtonTemplate")
	cc_cmb:SetPoint("CENTER", 28, 0)
	cc_cmb.Text:SetText("Cranial Concoction")
	cc_cmb.SetValue = function(_, value)
		self.db.ccOptions[1] = (value == "1")
		DDB.isMacro()
	end
	cc_cmb:SetChecked(self.db.ccOptions[1])
	
	------------------------------ FLASK OF ARDENDEW -------------------------------------------------
	
	local falsk_of_ardendew = CreateFrame("Frame", nil, cranial_concoction)
	falsk_of_ardendew:SetPoint("CENTER", 0, -36)
	falsk_of_ardendew:SetSize(32, 32)
	falsk_of_ardendew.tex = falsk_of_ardendew:CreateTexture()
	falsk_of_ardendew.tex:SetAllPoints(falsk_of_ardendew)
	falsk_of_ardendew.tex:SetTexture("132803")
	
	falsk_of_ardendew:SetScript("OnEnter", function()
		GameTooltip:SetOwner(falsk_of_ardendew, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:173762")
		GameTooltip:Show()
		end)
	falsk_of_ardendew:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local foa_cmb = CreateFrame("CheckButton", nil, falsk_of_ardendew, "InterfaceOptionsCheckButtonTemplate")
	foa_cmb:SetPoint("CENTER", 28, 0)
	foa_cmb.Text:SetText("Flask of Ardendew")
	foa_cmb.SetValue = function(_, value)
		self.db.foaOptions[1] = (value == "1")
		DDB.isMacro()
	end
	foa_cmb:SetChecked(self.db.foaOptions[1])
	
	------------------------------ INFUSED MUCK WATER -------------------------------------------------
	
	local infused_muck_water = CreateFrame("Frame", nil, falsk_of_ardendew)
	infused_muck_water:SetPoint("CENTER", 0, -36)
	infused_muck_water:SetSize(32, 32)
	infused_muck_water.tex = infused_muck_water:CreateTexture()
	infused_muck_water.tex:SetAllPoints(infused_muck_water)
	infused_muck_water.tex:SetTexture("132808")
	
	infused_muck_water:SetScript("OnEnter", function()
		GameTooltip:SetOwner(infused_muck_water, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:179993")
		GameTooltip:Show()
		end)
	infused_muck_water:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local imw_cmb = CreateFrame("CheckButton", nil, infused_muck_water, "InterfaceOptionsCheckButtonTemplate")
	imw_cmb:SetPoint("CENTER", 28, 0)
	imw_cmb.Text:SetText("Infused Muck Water")
	imw_cmb.SetValue = function(_, value)
		self.db.imwOptions[1] = (value == "1")
		DDB.isMacro()
	end
	imw_cmb:SetChecked(self.db.imwOptions[1])
	
	------------------------------ PURIFIED SKYSPRING WATER -------------------------------------------------
	
	local purified_skyspring_water = CreateFrame("Frame", nil, infused_muck_water)
	purified_skyspring_water:SetPoint("CENTER", 0, -36)
	purified_skyspring_water:SetSize(32, 32)
	purified_skyspring_water.tex = purified_skyspring_water:CreateTexture()
	purified_skyspring_water.tex:SetAllPoints(purified_skyspring_water)
	purified_skyspring_water.tex:SetTexture("236211")
	
	purified_skyspring_water:SetScript("OnEnter", function()
		GameTooltip:SetOwner(purified_skyspring_water, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:174281")
		GameTooltip:Show()
		end)
	purified_skyspring_water:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local psw_cmb = CreateFrame("CheckButton", nil, purified_skyspring_water, "InterfaceOptionsCheckButtonTemplate")
	psw_cmb:SetPoint("CENTER", 28, 0)
	psw_cmb.Text:SetText("Purified Skyspring Water")
	psw_cmb.SetValue = function(_, value)
		self.db.pswOptions[1] = (value == "1")
		DDB.isMacro()
	end
	psw_cmb:SetChecked(self.db.pswOptions[1])
	
	------------------------------ SABLE "SOUP" -------------------------------------------------
	
	local sable_soup = CreateFrame("Frame", nil, purified_skyspring_water)
	sable_soup:SetPoint("CENTER", 0, -36)
	sable_soup:SetSize(32, 32)
	sable_soup.tex = sable_soup:CreateTexture()
	sable_soup.tex:SetAllPoints(sable_soup)
	sable_soup.tex:SetTexture("1500959")
	
	sable_soup:SetScript("OnEnter", function()
		GameTooltip:SetOwner(sable_soup, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:187911")
		GameTooltip:Show()
		end)
	sable_soup:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local sas_cmb = CreateFrame("CheckButton", nil, sable_soup, "InterfaceOptionsCheckButtonTemplate")
	sas_cmb:SetPoint("CENTER", 28, 0)
	sas_cmb.Text:SetText('Sable "Soup"')
	sas_cmb.SetValue = function(_, value)
		self.db.sasOptions[1] = (value == "1")
		DDB.isMacro()
	end
	sas_cmb:SetChecked(self.db.sasOptions[1])
	
	------------------------------ SLUSHY WATER -------------------------------------------------
	
	local slushy_water = CreateFrame("Frame", nil, sable_soup)
	slushy_water:SetPoint("CENTER", 0, -36)
	slushy_water:SetSize(32, 32)
	slushy_water.tex = slushy_water:CreateTexture()
	slushy_water.tex:SetAllPoints(slushy_water)
	slushy_water.tex:SetTexture("132807")
	
	slushy_water:SetScript("OnEnter", function()
		GameTooltip:SetOwner(slushy_water, "ANCHOR_CURSOR")
		GameTooltip:SetHyperlink("item:184201")
		GameTooltip:Show()
		end)
	slushy_water:SetScript("OnLeave", function()
		GameTooltip:Hide()
		end)

	local slw_cmb = CreateFrame("CheckButton", nil, slushy_water, "InterfaceOptionsCheckButtonTemplate")
	slw_cmb:SetPoint("CENTER", 28, 0)
	slw_cmb.Text:SetText("Slushy Water")
	slw_cmb.SetValue = function(_, value)
		self.db.slwOptions[1] = (value == "1")
		DDB.isMacro()
	end
	slw_cmb:SetChecked(self.db.slwOptions[1])
	
end