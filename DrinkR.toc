## Interface: 80300
## Title: DrinkR
## Notes: Provides a drinking macro and keeps it updated
## Author: Percywal
## Version: 0.2.0
## SavedVariables: DrinkRDB

DrinkRDB.lua
DrinkRConfig.lua
DrinkR.lua
ExpansionDrinks\Mage_drinks.lua
ExpansionDrinks\Shadowlands_drinks.lua
