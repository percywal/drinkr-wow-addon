local addonName, DDB = ...

-- Local key table to pair with DDB.drink_table --
local keys = {
	"cmbOption", 	
	"cmpOption",	
	"adOptions", 	
	"abtOptions", 	
	"batOptions", 	
	"cosOptions", 	
	"capOptions", 	
	"efsOptions", 	
	"etOptions", 	
	"ltmOptions", 	
	"rfOptions", 	
	"swOptions", 	
	"ssOptions", 	
	"sssOptions", 	
	"bjOptions", 	
	"csOptions", 	
	"ccOptions", 	
	"foaOptions", 	
	"imwOptions", 	
	"pswOptions", 	
	"sasOptions", 	
	"slwOptions",
}

-- Parses keys and DDB.drink_table tables together and fills up drinks --
local function parseTablesForDrinks()
	drinks = {}
	for id, key in ipairs(keys) do
		if DDB.drink_table[key][1] == true then
			table.insert(drinks, DDB.drink_table[key][2])
		end
	end
end

-- Refreshes macro based on drinks and available refreshments --
local function refreshMacro()
	parseTablesForDrinks()
	for drink = 1, #drinks do
		if GetItemCount(drinks[drink]) > 0 then
			EditMacro("0DRKR", "0DRKR", nil, "#showtooltip\n/use " .. drinks[drink])
			break
		end
	end
end

-- This function triggers the macro refreshments and saved_variable DrinkRDB --
function DDB.isMacro()
	if GetMacroBody("0DRKR") == nil then
		CreateMacro("0DRKR", "INV_MISC_QUESTIONMARK", "1", nil)
	end
	refreshMacro()
	DrinkRDB = DDB.drink_table
end

-- Registering the events which fires isMacro() --
local frame = CreateFrame("FRAME", "event_frame");
frame:RegisterEvent("BAG_UPDATE");
frame:RegisterEvent("PLAYER_ENTERING_WORLD");
local function onEvent(self, event, ...)
	if (event == "PLAYER_ENTERING_WORLD" or event == "BAG_UPDATE") then
		DDB.isMacro()
	end
end
frame:SetScript("OnEvent", onEvent);

-- Slash commands for testing purpouses --

SLASH_TEST1 = "/test1"
SLASH_TEST2 = "/addontest1"

SlashCmdList["TEST"] = function(msg)
	print(GetItemIcon(178545))
	print(GetMacroBody("0DRKR"))
	--print(DrinkRDB.cmbOption[1])
	--print(DrinkRDB.cmbOption[2])
	--print(macroToParse)
	--print(drinks)
	--print("TestEditMAcro")
	--refreshMacro()
	--EditMacro("foodie", "foodie", nil, "/use Ethereal Pomegranate")
	--EditMacro("0DRKR", "0DRKR", nil, "/use Ethereal Pomegranate")
end 

